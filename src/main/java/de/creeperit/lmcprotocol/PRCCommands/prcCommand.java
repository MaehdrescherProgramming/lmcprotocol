package de.creeperit.lmcprotocol.PRCCommands;

public interface prcCommand {

    String getShort();

    String getLong();

}
