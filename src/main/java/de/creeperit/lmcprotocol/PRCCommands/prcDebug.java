package de.creeperit.lmcprotocol.PRCCommands;

public class prcDebug implements prcCommand {
    @Override
    public String getShort() {
        return "deb";
    }

    @Override
    public String getLong() {
        return "debug";
    }
}
