package de.creeperit.lmcprotocol.PRCCommands;

public class prcMessage implements prcCommand {
    @Override
    public String getShort() {
        return "msg";
    }

    @Override
    public String getLong() {
        return "message";
    }
}
