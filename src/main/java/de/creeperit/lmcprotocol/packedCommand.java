package de.creeperit.lmcprotocol;

import de.creeperit.lmcprotocol.ArgumentTypes.prcArg;
import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class packedCommand {

    prcCommand command;
    packedArgs args;

    packedCommand(String packedCommandString) {
        //decode Command
        command = prcCommandManager.getCommandByShortName(packedCommandString.substring(0, 3));
        prcArg arg = ArgumentManager.getArgByChar(packedCommandString.charAt(3));
        String[] strings = arg.decode(packedCommandString.substring(4));
        args = new packedArgs(arg, strings);
    }

    public packedCommand(prcCommand command, packedArgs args) {
        this.command = command;
        this.args = args;
    }

    public packedCommand(prcCommand command) {
        this.command = command;
        this.args = ArgumentManager.createArgumentFromString(""); //TODO: Verbessern, ist nur eine Übergangslösung
    }

    public prcCommand getCommand() {
        return command;
    }

    public prcArg getArgumentType() {
        return args.getArgType();
    }

    public String[] getStrings() {
        return args.getArguments();
    }

    public String getStringAtPosition(int position) {
        return args.getArgumentAtPosition(position);
    }

    public String getFullCommand() {
        return command.getShort() + args.getArgType().getArgID() + args.getEncodedArgs();
    }

}
