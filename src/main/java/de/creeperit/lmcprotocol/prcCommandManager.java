package de.creeperit.lmcprotocol;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class prcCommandManager {

    public static prcCommand getCommandByShortName(String shortc) { // META Command
        return getCommandByShortName(shortc, true);
    }

    public static prcCommand getCommandByShortName(String shortc, boolean caseSensitive) {
        for (prcCommand prccommand2 : Protocol.prcCommandList) {
            if (!caseSensitive && shortc.equals(prccommand2.getShort())) return prccommand2;
            if (caseSensitive && shortc.toLowerCase().equals(prccommand2.getShort().toLowerCase()))
                return prccommand2;
        }
        return null;
    }

    public static prcCommand getCommandByLongName(String longc) { // META Command
        return getCommandByLongName(longc, true);
    }

    public static prcCommand getCommandByLongName(String longc, boolean caseSensitive) {
        for (prcCommand prccommand2 : Protocol.prcCommandList) {
            if (!caseSensitive && longc.equals(prccommand2.getLong())) return prccommand2;
            if (caseSensitive & longc.toLowerCase().equals(prccommand2.getLong().toLowerCase()))
                return prccommand2;
        }
        return null;
    }

}
