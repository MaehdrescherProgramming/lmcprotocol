package de.creeperit.lmcprotocol;

import de.creeperit.lmcprotocol.ArgumentTypes.argD;
import de.creeperit.lmcprotocol.ArgumentTypes.prcArg;

import java.util.ArrayList;

public class ArgumentManager {

    public static packedArgs createArgumentFromArrayList(ArrayList<String> raw) {
        // choose the best ArgumentType to pack the arguments
        // if they are packed, they can easily send whit an command to the Server / Client

        //TODO: Fancy process to choose which ArgumentType is the best to use now
        prcArg selectedArgType = new argD();
        // We capsule the arguments and argumentType to give it back over return
        return new packedArgs(selectedArgType, raw);
    }

    public static packedArgs createArgumentFromString(String raw) {
        ArrayList<String> convertedString = new ArrayList<>();
        convertedString.add(raw);
        return createArgumentFromArrayList(convertedString);
    }

    public static prcArg getArgByChar(char c) {
        for (prcArg arg : Protocol.prcArgList) {
            if (arg.getArgID() == c) return arg;
        }
        return null;
    }

}
