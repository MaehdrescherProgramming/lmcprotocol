package de.creeperit.lmcprotocol.ArgumentTypes;

import java.util.ArrayList;

public interface prcArg {
    char getArgID();                                // the ID of this argumentType

    String encode(ArrayList<String> strings); // get a ArgumentsString

    String encode(String[] strings);

    String[] decode(String string);  // get Strings from ArgumentString
}
