package de.creeperit.lmcprotocol.ArgumentTypes;

import java.util.ArrayList;

public class argD implements prcArg { // Just send everything to the Server and theoretical dos them -> Why not?

    String argSplitter = "&:///:&"; // DO not use | for splitting, java doesn't like that

    @Override
    public char getArgID() {
        return 'D';
    }

    @Override
    public String encode(String[] strings) {
        String encodedString = "";
        boolean i = false;
        for (String s : strings) {
            if (i) encodedString += argSplitter + s;
            if (!i) {
                encodedString = s;
                i = true;
            }
        }
        return encodedString;
    }

    @Override
    public String encode(ArrayList<String> strings) {
        return encode(strings.toArray(new String[0]));
    }

    @Override
    public String[] decode(String string) {
        return string.split(argSplitter);
    }
}
