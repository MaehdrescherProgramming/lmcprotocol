package de.creeperit.lmcprotocol;

import de.creeperit.lmcprotocol.ArgumentTypes.prcArg;

import java.util.ArrayList;

public class packedArgs { // This Class is a container for an argumentType and the arguments (string)
    prcArg argType;
    String[] arguments;

    packedArgs(prcArg usedArgumentType, ArrayList<String> arguments) {
        argType = usedArgumentType;
        this.arguments = arguments.toArray(new String[0]);
    }

    public packedArgs(prcArg arg, String[] strings) {
        argType = arg;
        this.arguments = strings;
    }

    public prcArg getArgType() {
        return argType;
    }

    public String[] getArguments() {
        return arguments;
    }

    public String getArgumentAtPosition(int position) {
        return arguments[position];
    }

    public String getEncodedArgs() {
        return argType.encode(arguments);
    }
}
