package de.creeperit.lmcprotocol;

import de.creeperit.lmcprotocol.PRCCommands.prcCommand;

public class CommandBuilder {

    public static packedCommand build(prcCommand command, packedArgs packedArgs) {
        // add the short command, and the encoded arguments
        return new packedCommand(command, packedArgs);
    }

    public static packedCommand build(prcCommand command) {
        return new packedCommand(command);
    }

    public static packedCommand getPackedCommandFromString(String s) {
        return new packedCommand(s);
    }

}
