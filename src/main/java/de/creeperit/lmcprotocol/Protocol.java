package de.creeperit.lmcprotocol;

import de.creeperit.lmcprotocol.ArgumentTypes.argD;
import de.creeperit.lmcprotocol.ArgumentTypes.prcArg;
import de.creeperit.lmcprotocol.PRCCommands.prcCommand;
import de.creeperit.lmcprotocol.PRCCommands.prcDebug;
import de.creeperit.lmcprotocol.PRCCommands.prcMessage;
import de.creeperit.lmcprotocol.PRCCommands.prcTest;

import java.util.ArrayList;

public class Protocol {

    public static final String version = "28.02.2020";
    public static final String protocolVersion = "Cl3";
    public static ArrayList<prcCommand> prcCommandList;
    static ArrayList<prcArg> prcArgList;

    public static void init() { // Have to be started before any commands can be sent to anyone
        prcCommandList = new ArrayList<prcCommand>();
        prcCommandList.add(new prcTest());
        prcCommandList.add(new prcDebug());
        prcCommandList.add(new prcMessage());
        prcArgList = new ArrayList<prcArg>();
        prcArgList.add(new argD());
    }

    public static void addCommand(prcCommand Command) { // To modify the commandList to add more commands
        prcCommandList.add(Command);
    }

    public static boolean removeCommand(prcCommand command) { // To modify the commandList to remove not wanted commands
        if (prcCommandList.indexOf(command) > -1) {
            prcCommandList.remove(prcCommandList.indexOf(command));
            return true;
        }
        return false;
    }
}
